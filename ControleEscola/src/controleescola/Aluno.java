package controleescola;

public class Aluno {
    
    private String ra;
    private String nome;
    private String turno;
    private String status;    

    public Aluno(String ra, String nome) {
        this.ra = ra;
        this.nome = nome;
        this.turno = "Diurno";
        this.status = "Ativo";
    }

    public Aluno(String ra, String nome, String turno, String status) {
        this.ra = ra;
        this.nome = nome;
        this.turno = turno;
        this.status = status;
    }
    
    
    /*Metodos*/
            
    public void Mostrar() {
        System.out.println("RA do Aluno: " +ra);
        System.out.println("Nome do Aluno: " + nome);
        System.out.println("Turno Aluno: " + turno);
        System.out.println("Status do Aluno: " + status);
        
    }
    
    public String Trancar(String status){
        if (status.equals("Ativo")){
         status = "Suspenso";
        }
        return  status;
    }
        
    public String Destrancar(String status){
        if (status.equals("Suspenso")){
         status = "Ativo";
        }
        return  status;
    }
    
    public String Formar(String status){
        if (status.equals("Ativo")){
         status = "Egresso";
        }
        return  status;
    }
    
        public String Desistir(String status){
        if (status.equals("Ativo") || status.equals("Suspenso")){
            status = "Desistente";
        }
        return  status;
    }
        
    /* Geters*/
    public String getRa() {
        return ra;
    }

    public String getNome() {
        return nome;
    }

    public String getTurno() {
        return turno;
    }

    public String getStatus() {
        return status;
    }
    
}
