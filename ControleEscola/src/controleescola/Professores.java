package controleescola;
  
    public class Professores  extends Funcionario{
    private String areaAtuacao;

    
    public String getAreaAtuacao() {
        return areaAtuacao;
    }

    public Professores(String nome, String rg, double salario, double quantidadeDeHoraMes, String areaAtuacao) {
        super(nome, rg, salario, quantidadeDeHoraMes);
        this.nome = nome;
        this.rg = rg;
        this.salario = salario;
        this.quantidadeDeHoraMes = quantidadeDeHoraMes;
        this.areaAtuacao = areaAtuacao;
    }
    public Professores(String nome, String rg) {
        super(nome, rg);
        this.nome = nome;
        this.rg = rg;
        this.salario = 0;
        this.quantidadeDeHoraMes = 0;
        this.areaAtuacao = "Exatas";
    }
    
  public void Mostrar() {
        System.out.println("Nome do Professor: " + nome);
        System.out.println("RG do Professor: "+ rg);
        System.out.println("Salario do professor: " + salario);
        System.out.println("Horas  trabalhadas no mes: "+ quantidadeDeHoraMes);
        System.out.println("Area de atuação: " + areaAtuacao);
  }
    
    
    
}
    
