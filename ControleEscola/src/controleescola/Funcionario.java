package controleescola;

public class Funcionario  {
    
    protected String nome;
    protected String rg;
    protected double salario;
    protected double quantidadeDeHoraMes;

    public Funcionario(String nome, String rg, double salario, double quantidadeDeHoraMes) {
        this.nome = nome;
        this.rg = rg;
        this.salario = salario;
        this.quantidadeDeHoraMes = quantidadeDeHoraMes;
    }
    public Funcionario(String nome, String rg) {
        this.nome = nome;
        this.rg = rg;
        this.salario = 0;
        this.quantidadeDeHoraMes = 0;
    }
    
    public void Mostrar() {
        System.out.println("Nome do Funcionario: " + nome);
        System.out.println("RG do Funcionario: " + rg);
        System.out.println("Salario do Funcionario: " + salario);
        System.out.println("Horas trabalhadas no mes: " + quantidadeDeHoraMes);
    }
    /*Geters*/
        
    public String getNome() {
        return nome;
    }

    public String getRg() {
        return rg;
    }

    public double getSalario() {
        return salario;
    }

    public double getQuantidadeDeHoraMes() {
        return quantidadeDeHoraMes;
    }
   
    
}
